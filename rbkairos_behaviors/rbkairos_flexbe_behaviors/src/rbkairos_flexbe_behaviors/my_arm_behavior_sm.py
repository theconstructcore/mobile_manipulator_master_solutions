#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from rbkairos_flexbe_states.control_robot_arm import MoveArmState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu Feb 20 2020
@author: Rick
'''
class my_arm_behaviorSM(Behavior):
	'''
	blab bla bla
	'''


	def __init__(self):
		super(my_arm_behaviorSM, self).__init__()
		self.name = 'my_arm_behavior'

		# parameters of this behavior
		self.add_parameter('_joint1', 0)
		self.add_parameter('_joint2', 0)
		self.add_parameter('_joint3', 0)
		self.add_parameter('_joint4', 0)
		self.add_parameter('_joint5', 0)
		self.add_parameter('_joint6', 0)

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		# x:30 y:319, x:130 y:319
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:30 y:40
			OperatableStateMachine.add('moving the arm',
										MoveArmState(joint1=self._joint1, joint2=self._joint2, joint3=self._joint3, joint4=self._joint4, joint5=self._joint5, joint6=self._joint6),
										transitions={'failed': 'failed', 'done': 'finished'},
										autonomy={'failed': Autonomy.Off, 'done': Autonomy.Off})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
