#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from rbkairos_flexbe_states.navigation_goal import NavGoalState
from rbkairos_flexbe_states.place_object import PlaceObjectState
from rbkairos_flexbe_states.pick_object import PickObjectState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu Feb 20 2020
@author: Rick T
'''
class warehousebehaviorrealSM(Behavior):
	'''
	The full behavior of the master of mobile manipulators, with real robot mods when needed
	'''


	def __init__(self):
		super(warehousebehaviorrealSM, self).__init__()
		self.name = 'warehouse behavior real'

		# parameters of this behavior
		self.add_parameter('my_pick_location', 'table')
		self.add_parameter('my_realease_location', 'basket')

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		# x:406 y:396, x:209 y:485
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:30 y:40
			OperatableStateMachine.add('Go to table',
										NavGoalState(tag=self.my_pick_location),
										transitions={'failed': 'failed', 'done': 'Grasp'},
										autonomy={'failed': Autonomy.Off, 'done': Autonomy.Off})

			# x:431 y:244
			OperatableStateMachine.add('Release',
										PlaceObjectState(),
										transitions={'failed': 'failed', 'done': 'Go to table'},
										autonomy={'failed': Autonomy.Off, 'done': Autonomy.Off},
										remapping={'cube': 'cube', 'fail_ct': 'fail_ct', 'pick_result': 'pick_result'})

			# x:427 y:92
			OperatableStateMachine.add('Go to basket',
										NavGoalState(tag=self.my_realease_location),
										transitions={'failed': 'failed', 'done': 'Release'},
										autonomy={'failed': Autonomy.Off, 'done': Autonomy.Off})

			# x:250 y:54
			OperatableStateMachine.add('Grasp',
										PickObjectState(),
										transitions={'failed': 'failed', 'done': 'Go to basket'},
										autonomy={'failed': Autonomy.Off, 'done': Autonomy.Off},
										remapping={'cube': 'cube', 'fail_ct': 'fail_ct', 'pick_result': 'pick_result'})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
