#!/usr/bin/env python
import rospy

from flexbe_core import EventState, Logger
from flexbe_core.proxy import ProxyActionClient
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose
import rosparam

class NavGoalState(EventState):
    '''
    Example for a state to demonstrate which functionality is available for state implementation.
    This example lets the behavior wait until the given target_time has passed since the behavior has been started.

    -- target_time 	float 	Time which needs to have passed since the behavior started.

    <= continue 			Given time has passed.
    <= failed 				Example for a failure outcome.

    '''

    def __init__(self, tag):
        # Declare outcomes, input_keys, and output_keys by calling the super constructor with the corresponding arguments.
        super(NavGoalState, self).__init__(outcomes = ['failed', 'done'])

        # Tag containing the name of the goal destination
        self.tag = tag

        self._topic = '/move_base'

        # Create the action client passing it the spin_topic_name and msg_type
        self._client = ProxyActionClient({self._topic: MoveBaseAction}) # pass required clients as dict (topic: type)

        # It may happen that the action client fails to send the action goal.
        self._error = False

        # Tag containing the name of the goal destination
        #self.tag = goal_label


    def execute(self, userdata):
        # This method is called periodically while the state is active.
        # Main purpose is to check state conditions and trigger a corresponding outcome.
        # If no outcome is returned, the state will stay active.

        # Check if the client failed to send the goal.
        if self._error:
            return 'failed'

        # Check if the action has been finished
        if self._client.has_result(self._topic):
            result = self._client.get_result(self._topic)

            # Based on the result, decide which outcome to trigger.
            if result:
                return 'done'
            else:
                return 'failed'

        # Check if there is any feedback
        if self._client.has_feedback(self._topic):
            feedback = self._client.get_feedback(self._topic)
            Logger.loginfo("Current Position is: %s" % str(feedback.base_position.pose.position))

        # If the action has not yet finished, no outcome will be returned and the state stays active.


    def on_enter(self, userdata):
        # This method is called when the state becomes active, i.e. a transition from another state to this one is taken.
        # It is primarily used to start actions which are associated with this state.

        # The following code is just for illustrating how the behavior logger works.
        # Text logged by the behavior logger is sent to the operator and displayed in the GUI.

        goal=MoveBaseGoal()
        goal_tmp = Pose()

        goal_tmp.position.x=rosparam.get_param(self.tag+'/position/x')
        goal_tmp.position.y=rosparam.get_param(self.tag+'/position/y')
        goal_tmp.position.z=rosparam.get_param(self.tag+'/position/z')
        goal_tmp.orientation.x=rosparam.get_param(self.tag+'/orientation/x')
        goal_tmp.orientation.y=rosparam.get_param(self.tag+'/orientation/y')
        goal_tmp.orientation.z=rosparam.get_param(self.tag+'/orientation/z')
        goal_tmp.orientation.w=rosparam.get_param(self.tag+'/orientation/w')

        goal.target_pose.pose=goal_tmp
        #goal.target_pose.header.frame_id='map'
        goal.target_pose.header.frame_id='robot_map'

        # Send the goal.
        self._error = False # make sure to reset the error state since a previous state execution might have failed
        try:
            self._client.send_goal(self._topic, goal)
        except Exception as e:
            # Since a state failure not necessarily causes a behavior failure, it is recommended to only print warnings, not errors.
            # Using a linebreak before appending the error log enables the operator to collapse details in the GUI.
            Logger.logwarn('Failed to send the navigation goal:\n%s' % str(e))
            self._error = True


    def on_exit(self, userdata):
        # Make sure that the action is not running when leaving this state.
        # A situation where the action would still be active is for example when the operator manually triggers an outcome.

        if not self._client.has_result(self._topic):
            self._client.cancel(self._topic)
            Logger.loginfo('Cancelled active navigation goal.')


    def on_start(self):
        # This method is called when the behavior is started.
        # If possible, it is generally better to initialize used resources in the constructor
        # because if anything failed, the behavior would not even be started.

        # In this example, we use this event to set the correct start time.
        Logger.loginfo("NavGoal State READY!")


    def on_stop(self):
        # This method is called whenever the behavior stops execution, also if it is cancelled.
        # Use this event to clean up things like claimed resources.

        #same implementation as on exit
        if not self._client.has_result(self._topic):
            self._client.cancel(self._topic)
            Logger.loginfo('Cancelled active navigation goal.')