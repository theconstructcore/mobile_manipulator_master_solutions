#!/usr/bin/env python
import rospy

from flexbe_core import EventState, Logger
from moveit_python import MoveGroupInterface
from moveit_msgs.msg import MoveItErrorCodes


class MoveArmState(EventState):
    '''
    Example for a state to demonstrate which functionality is available for state implementation.
    This example lets the behavior wait until the given target_time has passed since the behavior has been started.

    -- target_time 	float 	Time which needs to have passed since the behavior started.

    <= continue 			Given time has passed.
    <= failed 				Example for a failure outcome.

    '''

    def __init__(self, joint1, joint2, joint3, joint4, joint5, joint6):
        # Declare outcomes, input_keys, and output_keys by calling the super constructor with the corresponding arguments.
        super(MoveArmState, self).__init__(outcomes = ['failed', 'done'])

        self._joint1 = joint1
        self._joint2 = joint2
        self._joint3 = joint3
        self._joint4 = joint4
        self._joint5 = joint5
        self._joint6 = joint6

        # Store state parameter for later use.
        self.joints = ["robot_arm_elbow_joint", "robot_arm_shoulder_lift_joint", "robot_arm_shoulder_pan_joint",
                  "robot_arm_wrist_1_joint", "robot_arm_wrist_2_joint", "robot_arm_wrist_3_joint"]
        self.pose = [2.82, -3.07, 0.0, -2.83, 0.0, 0.0]
        self.move_group = MoveGroupInterface("arm", "rbkairos_base_footprint")

    def execute(self, userdata):
        while not rospy.is_shutdown():
            result = self.move_group.moveToJointPosition(self.joints, self.pose, 0.02)
            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                return 'done'

    def on_enter(self, userdata):
        # This method is called when the state becomes active, i.e. a transition from another state to this one is taken.
        # It is primarily used to start actions which are associated with this state.

        # The following code is just for illustrating how the behavior logger works.
        # Text logged by the behavior logger is sent to the operator and displayed in the GUI.

        Logger.loginfo("MoveArm STARTED!")


    def on_exit(self, userdata):
        # This method is called when an outcome is returned and another state gets active.
        # It can be used to stop possibly running processes started by on_enter.

        Logger.loginfo("MoveArm ENDED!")


    def on_start(self):
        # This method is called when the behavior is started.
        # If possible, it is generally better to initialize used resources in the constructor
        # because if anything failed, the behavior would not even be started.

        # In this example, we use this event to set the correct start time.
        Logger.loginfo("MoveArm READY!")


    def on_stop(self):
        # This method is called whenever the behavior stops execution, also if it is cancelled.
        # Use this event to clean up things like claimed resources.

        Logger.loginfo("MoveArm STOPPED!")