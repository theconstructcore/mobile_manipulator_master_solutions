#!/usr/bin/env python
import rospy

from flexbe_core import EventState, Logger
from moveit_msgs.msg import MoveItErrorCodes
from moveit_python import MoveGroupInterface

from grasping_client import GraspingClient


class PickObjectState(EventState):

    def __init__(self):
        # Declare outcomes, input_keys, and output_keys by calling the super constructor with the corresponding arguments.
        super(PickObjectState, self).__init__(outcomes = ['failed', 'done'], output_keys = ['cube','fail_ct','pick_result'])

        self.grasping_client = GraspingClient()
        self.cube_in_grapper = False
        self.i=False


    def execute(self, userdata):
        # This method is called periodically while the state is active.
        # Main purpose is to check state conditions and trigger a corresponding outcome.
        # If no outcome is returned, the state will stay active.

        while not rospy.is_shutdown() and self.i == False:
            self.i=True

            # Get block to pick
            fail_ct = 0
            while not rospy.is_shutdown() and not self.cube_in_grapper:
                rospy.loginfo("Picking object...")
                self.grasping_client.updateScene()
                print "Going to Enter getGraspableObject..."
                cube, grasps = self.grasping_client.getGraspableObject()
                userdata.cube = cube
                print "Exited getGraspableObject..."
                if cube == None:
                    rospy.logwarn("Perception failed.")
                    self.grasping_client.stow()
                    continue

                # Pick the block
                print "Call pick function..."
                success, pick_result = self.grasping_client.pick(cube, grasps)
                userdata.pick_result = pick_result
                #if self.grasping_client.pick(cube, grasps):
                if success:

                    self.cube_in_grapper = True
                    userdata.fail_ct = fail_ct
                    #break
                    return 'done'
                rospy.logwarn("Grasping failed.")
                self.grasping_client.stow()
                if fail_ct > 15:
                    fail_ct = 0
                    #break
                    return 'failed'
                fail_ct += 1

    def on_enter(self, userdata):
        # This method is called when the state becomes active, i.e. a transition from another state to this one is taken.
        # It is primarily used to start actions which are associated with this state.

        # The following code is just for illustrating how the behavior logger works.
        # Text logged by the behavior logger is sent to the operator and displayed in the GUI.

        self.grasping_client.stow()

        Logger.loginfo("PickObject STARTED!")

    def on_exit(self, userdata):
        # This method is called when an outcome is returned and another state gets active.
        # It can be used to stop possibly running processes started by on_enter.

        Logger.loginfo("PickObject ENDED!")


    def on_start(self):
        # This method is called when the behavior is started.
        # If possible, it is generally better to initialize used resources in the constructor
        # because if anything failed, the behavior would not even be started.

        # In this example, we use this event to set the correct start time.
        Logger.loginfo("PickObject READY!")


    def on_stop(self):
        # This method is called whenever the behavior stops execution, also if it is cancelled.
        # Use this event to clean up things like claimed resources.

        Logger.loginfo("PickObject STOPPED!")