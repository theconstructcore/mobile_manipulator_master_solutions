#!/usr/bin/env python
import rospy

from flexbe_core import EventState, Logger
from moveit_msgs.msg import MoveItErrorCodes
from moveit_python import MoveGroupInterface

from grasping_client import GraspingClient
from geometry_msgs.msg import PoseStamped


class PlaceObjectState(EventState):

    def __init__(self):
        # Declare outcomes, input_keys, and output_keys by calling the super constructor with the corresponding arguments.
        super(PlaceObjectState, self).__init__(outcomes = ['failed', 'done'], input_keys = ['cube', 'fail_ct','pick_result'])

        self.grasping_client = GraspingClient()
        self.cube_in_grapper = True

    def execute(self, userdata):
        # This method is called periodically while the state is active.
        # Main purpose is to check state conditions and trigger a corresponding outcome.
        # If no outcome is returned, the state will stay active.

        cube = userdata.cube
        fail_ct = userdata.fail_ct
        pick_result = userdata.pick_result

        while not rospy.is_shutdown() and self.cube_in_grapper:
            rospy.loginfo("Placing object...")
            pose = PoseStamped()
            pose.pose = cube.primitive_poses[0]
            pose.pose.position.y += -0.2
            pose.pose.position.z += 0.02

            pose.header.frame_id = cube.header.frame_id
            if self.grasping_client.place(cube, pose, pick_result):
                self.cube_in_grapper = False
                #break
                return 'done'
            rospy.logwarn("Placing failed.")
            #grasping_client.intermediate_stow()
            self.grasping_client.stow()
            if fail_ct > 15:
                fail_ct = 0
                #break
                return 'failed'
            fail_ct += 1

    def on_enter(self, userdata):
        # This method is called when the state becomes active, i.e. a transition from another state to this one is taken.
        # It is primarily used to start actions which are associated with this state.

        # The following code is just for illustrating how the behavior logger works.
        # Text logged by the behavior logger is sent to the operator and displayed in the GUI.

        Logger.loginfo("PlaceObject STARTED!")

    def on_exit(self, userdata):
        # This method is called when an outcome is returned and another state gets active.
        # It can be used to stop possibly running processes started by on_enter.

        self.grasping_client.stow()

        Logger.loginfo("PlaceObject ENDED!")


    def on_start(self):
        # This method is called when the behavior is started.
        # If possible, it is generally better to initialize used resources in the constructor
        # because if anything failed, the behavior would not even be started.

        # In this example, we use this event to set the correct start time.
        Logger.loginfo("PlaceObject READY!")


    def on_stop(self):
        # This method is called whenever the behavior stops execution, also if it is cancelled.
        # Use this event to clean up things like claimed resources.

        Logger.loginfo("PlaceObject STOPPED!")