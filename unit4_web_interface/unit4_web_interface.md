# Here you can find all the commands and notes for all the units

---

## Unit 0: Demo

roslaunch course_web_dev_ros web.launch
-->Rosbridge WebSocket server started on port 9090

rosbridge_address
-->wss://i-060ab572cac16bda6.robotigniteacademy.com/rosbridge/

"Connect using the python form below ijypter notebook"
%%HTML
<div style="width:100%;">
<iframe src="https://s3.eu-west-1.amazonaws.com/readme.theconstructsim.com/__others__/index.html" style="min-width:100%; height:800px; border:0" />
</div>


![demo_course_web](images/demo_course_web.png)