let vueApp = new Vue({
    el: "#vueApp",
    data: {
        // ros connection
        ros: null,
        rosbridge_address: 'wss://i-002ae4435c825fbfe.robotigniteacademy.com/rosbridge/',
        connected: false,
        // page content
        menu_title: 'Connection',
        // dragging data
        dragging: false,
        x: 'no',
        y: 'no',
        dragCircleStyle: {
            margin: '0px',
            top: '0px',
            left: '0px',
            display: 'none',
            width: '75px',
            height: '75px',
        },
        // joystick valules
        joystick: {
            vertical: 0,
            horizontal: 0,
        },
        // publisher
        pubInterval: null,
        // subscriber data
        position: { x: 0, y: 0, z: 0, },
        battery: 10,
        bateryBarStyle: "width:10%",
        batteryLow: false,
        mapViewer: null,
        mapGridClient: null,
        interval: null,
        // Custom map indicators
        robotMarkers: [],
        topics: [],
        zoom_factor: 10,
        map_x_axis_displacement: 0,
        map_y_axis_displacement: 0,
    },
    methods: {
        connect: function() {
            // define ROSBridge connection object
            this.ros = new ROSLIB.Ros({
                url: this.rosbridge_address
            })

            // define callbacks
            this.ros.on('connection', () => {
                this.connected = true
                console.log('Connection to ROSBridge established!')
                // define the publisher to cmd_vel
                this.pubInterval = setInterval(this.publish, 100)
                // define the subscriber to odometry
                let topic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/robot/robotnik_base_control/odom',
                    messageType: 'nav_msgs/Odometry'
                })
                topic.subscribe((message) => {
                    this.position = message.pose.pose.position
                    //console.log(message)
                })
                let topic_battery = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/robot/battery_level',
                    messageType: 'std_msgs/Int32'
                })
                topic_battery.subscribe((message) => {
                    this.battery = message.data
                    this.bateryBarStyle = "width:" + message.data + "%"
                    if (this.battery < 20) {
                        this.batteryLow = true
                    } else {
                        this.batteryLow = false
                    }
                    console.log(message)
                })
                this.setCamera()
                this.setCamera2()

                this.mapViewer = new ROS2D.Viewer({
                    divID: 'map',
                    width: 800,
                    height: 550
                })

                // Setup the map client.
                this.mapGridClient = new ROS2D.OccupancyGridClient({
                    ros: this.ros,
                    rootObject: this.mapViewer.scene,
                    continuous: true,
                })
                // Scale the canvas to fit to the map

                this.mapGridClient.on('change', () => {
                    this.update_zoom_map()
                })

                // Custom Map indicator
                // Setup the map client.
                var robotMarker = new ROS2D.NavigationArrow({
                    size: 0.25,
                    strokeSize: 0.05,
                    pulse: true,
                    fillColor: createjs.Graphics.getRGB(255, 0, 0, 1.0)
                });

                this.robotMarkers.push(robotMarker)
                var poseTopic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/robot/amcl_pose',
                    messageType: 'geometry_msgs/PoseWithCovarianceStamped'
                });
                this.topics.push(poseTopic)
                this.createFunc('subscribe', poseTopic, robotMarker)
                // We add al the robot indicators
                for (let i = 0; i < this.robotMarkers.length; i++) {
                    this.mapGridClient.rootObject.addChild(this.robotMarkers[i])
                }
                this.updateMapInterval = setInterval(this.update_zoom_map, 100)
            })
            this.ros.on('error', (error) => {
                console.log('Something went wrong when trying to connect')
                console.log(error)
            })
            this.ros.on('close', () => {
                this.connected = false
                console.log('Connection to ROSBridge was closed!')
                clearInterval(this.pubInterval)
                clearInterval(this.updateMapInterval)
                document.getElementById('divCamera').innerHTML = ''
                document.getElementById('divCamera2').innerHTML = ''
                document.getElementById('map').innerHTML = ''
            })
        },
        publish: function() {
            let topic = new ROSLIB.Topic({
                ros: this.ros,
                name: '/robot/cmd_vel',
                messageType: 'geometry_msgs/Twist'
            })
            let message = new ROSLIB.Message({
                linear: { x: this.joystick.vertical, y: 0, z: 0, },
                angular: { x: 0, y: 0, z: this.joystick.horizontal, },
            })
            topic.publish(message)
        },
        disconnect: function() {
            this.ros.close()
        },
        sendCommand: function() {
            let topic = new ROSLIB.Topic({
                ros: this.ros,
                name: '/robot/cmd_vel',
                messageType: 'geometry_msgs/Twist'
            })
            let message = new ROSLIB.Message({
                linear: { x: 1, y: 0, z: 0, },
                angular: { x: 0, y: 0, z: 0.5, },
            })
            topic.publish(message)
        },
        startDrag() {
            this.dragging = true
            this.x = this.y = 0
        },
        stopDrag() {
            this.dragging = false
            this.x = this.y = 'no'
            this.dragCircleStyle.display = 'none'
            this.resetJoystickVals()
        },
        doDrag(event) {
            if (this.dragging) {
                this.x = event.offsetX
                this.y = event.offsetY
                let ref = document.getElementById('dragstartzone')
                this.dragCircleStyle.display = 'inline-block'

                let minTop = ref.offsetTop - parseInt(this.dragCircleStyle.height) / 2
                let maxTop = minTop + 200
                let top = this.y + minTop
                this.dragCircleStyle.top = `${top}px`

                let minLeft = ref.offsetLeft - parseInt(this.dragCircleStyle.width) / 2
                let maxLeft = minLeft + 200
                let left = this.x + minLeft
                this.dragCircleStyle.left = `${left}px`

                this.setJoystickVals()
            }
        },
        setJoystickVals() {
            this.joystick.vertical = -1 * ((this.y / 200) - 0.5)
            this.joystick.horizontal = -1 * ((this.x / 200) - 0.5)
        },
        resetJoystickVals() {
            this.joystick.vertical = 0
            this.joystick.horizontal = 0
        },
        setCamera: function() {
            let without_wss = this.rosbridge_address.split('wss://')[1]
            console.log(without_wss)
            let domain = without_wss.split('/')[0]
            console.log(domain)
            let host = domain + '/cameras'
            let viewer = new MJPEGCANVAS.Viewer({
                divID: 'divCamera',
                host: host,
                width: 320,
                height: 240,
                topic: '/robot/front_rgbd_camera/rgb/image_raw',
                ssl: true,
            })
        },
        setCamera2: function() {
            let without_wss = this.rosbridge_address.split('wss://')[1]
            console.log(without_wss)
            let domain = without_wss.split('/')[0]
            console.log(domain)
            let host = domain + '/cameras'
            let viewer = new MJPEGCANVAS.Viewer({
                divID: 'divCamera2',
                host: host,
                width: 320,
                height: 240,
                topic: '/robot/wrist_rgbd/rgb/image_raw',
                ssl: true,
            })
        },

        update_zoom_map: function() {
            try {
                // we recreate the map canvas based on the zoom
                let dim_width = this.mapGridClient.currentGrid.width
                let dim_height = this.mapGridClient.currentGrid.height
                let grid_position_x = this.mapGridClient.currentGrid.pose.position.x
                let grid_position_y = this.mapGridClient.currentGrid.pose.position.y

                this.mapViewer.scaleToDimensions(dim_width / this.zoom_factor, dim_height / this.zoom_factor);
                this.mapViewer.shift((grid_position_x / this.zoom_factor) + this.map_x_axis_displacement, (grid_position_y / this.zoom_factor) - this.map_y_axis_displacement)

            }
            catch (err) {
                console.log("Map data not ready yet...")
            }

        },

        createFunc(handlerToCall, discriminator, robotMarker) {
            return discriminator.subscribe((pose) => {
                robotMarker.x = pose.pose.pose.position.x
                robotMarker.y = -pose.pose.pose.position.y

                var quaZ = pose.pose.pose.orientation.z
                var degreeZ = 0
                if (quaZ >= 0) {
                    degreeZ = quaZ / 1 * 180
                } else {
                    degreeZ = (-quaZ) / 1 * 180 + 180
                };
                //robotMarker.rotation = -degreeZ + 35
                robotMarker.rotation = -degreeZ

                this.map_x_axis_displacement = robotMarker.x
                this.map_y_axis_displacement = robotMarker.y
            })
        },

    },

    mounted() {
        // page is ready
        window.addEventListener('mouseup', this.stopDrag)
        // map
        this.intervalMap = setInterval(() => {
            if (this.ros != null && this.ros.isConnected) {
                this.ros.getNodes((data) => { }, (error) => { })
            }
        }, 10000)
    },
})