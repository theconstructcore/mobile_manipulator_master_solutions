#! /usr/bin/env python
import rospy
import time
import actionlib
from control_msgs.msg import GripperCommandGoal, GripperCommandFeedback, GripperCommandResult, GripperCommandAction
from geometry_msgs.msg import Twist
from std_srvs.srv import Trigger, TriggerRequest, TriggerResponse
from std_msgs.msg import Empty
"""
rosmsg show control_msgs/GripperCommandAction
control_msgs/GripperCommandActionGoal action_goal
  std_msgs/Header header
    uint32 seq
    time stamp
    string frame_id
  actionlib_msgs/GoalID goal_id
    time stamp
    string id
  control_msgs/GripperCommandGoal goal
    control_msgs/GripperCommand command
      float64 position
      float64 max_effort
control_msgs/GripperCommandActionResult action_result
  std_msgs/Header header
    uint32 seq
    time stamp
    string frame_id
  actionlib_msgs/GoalStatus status
    uint8 PENDING=0
    uint8 ACTIVE=1
    uint8 PREEMPTED=2
    uint8 SUCCEEDED=3
    uint8 ABORTED=4
    uint8 REJECTED=5
    uint8 PREEMPTING=6
    uint8 RECALLING=7
    uint8 RECALLED=8
    uint8 LOST=9
    actionlib_msgs/GoalID goal_id
      time stamp
      string id
    uint8 status
    string text
  control_msgs/GripperCommandResult result
    float64 position
    float64 effort
    bool stalled
    bool reached_goal
control_msgs/GripperCommandActionFeedback action_feedback
  std_msgs/Header header
    uint32 seq
    time stamp
    string frame_id
  actionlib_msgs/GoalStatus status
    uint8 PENDING=0
    uint8 ACTIVE=1
    uint8 PREEMPTED=2
    uint8 SUCCEEDED=3
    uint8 ABORTED=4
    uint8 REJECTED=5
    uint8 PREEMPTING=6
    uint8 RECALLING=7
    uint8 RECALLED=8
    uint8 LOST=9
    actionlib_msgs/GoalID goal_id
      time stamp
      string id
    uint8 status
    string text
  control_msgs/GripperCommandFeedback feedback
    float64 position
    float64 effort
    bool stalled
    bool reached_goal
"""
class RBKairosGripperASClass(object):

    # create messages that are used to publish feedback/result
    _feedback = BehaviorExecutionFeedback()
    _result = BehaviorExecutionResult()

    def __init__(self):
        # creates the action server
        self._as = actionlib.SimpleActionServer("/robot/gripper/gripper_controller", GripperCommandAction, self.goal_callback, False)
        self._as.start()
        self.rate = rospy.Rate(10)




        rospy.loginfo('Dummy Gripper Action server started')


    def check_gripper_services_available(self):
        
        rospy.loginfo("Waiting open svice")
        open_srv_name = "/robot/gripper/gripper_controller/open"
        rospy.wait_for_service()
        rospy.loginfo("Waiting close svice")
        close_srv_name = "/robot/gripper/gripper_controller/close"
        rospy.wait_for_service(close_srv_name)

        self.open_gripper_srv = rospy.ServiceProxy(open_srv_name, Trigger)
        self.close_gripper_srv = rospy.ServiceProxy(close_srv_name, Trigger)

        self.call_gripper_request = TriggerRequest()

        rospy.loginfo("Open And close srevice gripper ready")        
        
rosservice info /robot/gripper/gripper_controller/open
Node: /robot/gripper/gripper_controller
URI: rosrpc://rbkairos:40783
Type: std_srvs/Trigger


    def opengripper(self, value):
        
        try:
            response = self.open_gripper_srv(self.call_gripper_request)
            return response
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def goal_callback(self, goal):
        gripper_pose = goal.command.position
        rospy.loginfo("gripper_pose==>"+str(gripper_pose))

        # helper variables
        success = True
        goal_reached = False
        # define vars
        self._count = 0

        if gripper_pose == 0.0:
            increment = 0.01
            gripper_pose_now = 1.0
        elif gripper_pose == 1.0:
            increment = -0.01
            gripper_pose_now = 0.0
        else:
            goal_reached = True
            rospy.logerr("Only supported 1 or 0 pose goal")
        
        # perform task
        while not goal_reached and success and not rospy.is_shutdown():
            rospy.loginfo(self._count)
            self.rate.sleep()
            if self._as.is_preempt_requested():
                # cancelled
                rospy.loginfo('The goal has been cancelled/preempted at %s' % str(self._count))
                self._as.set_preempted()
                success = False
            else:
                # keep going
                gripper_pose_now += increment 
                self._feedback.current_state = str(gripper_pose)+str(self._count)

                self._feedback.result.position = gripper_pose_now            
                self._feedback.result.effort = 50.0
                self._feedback.result.stalled = False
                self._feedback.result.reached_goal = False

                self._as.publish_feedback(self._feedback)
            
            if gripper_pose_now >= 1.0 or gripper_pose_now <= 0.0:
                rospy.logerr("Goal Reached")
                goal_reached = True

        # return success
        if success:
            self._result.result.position = gripper_pose            
            self._result.result.effort = 50.0
            self._result.result.stalled = False
            self._result.result.reached_goal = True

            rospy.loginfo('result is %s' % str(self._result))
            self._as.set_succeeded(self._result)

class RBKairosGripperACClass(object):

    def __init__(self):
        self.rate = rospy.Rate(1)
        self.OPEN_VALUE = 1.0
        self.CLOSE_VALUE = 0.0
        self.as_name = "/robot/gripper/gripper_controller"
        self.client = actionlib.SimpleActionClient(self.as_name, GripperCommandAction)

        rosy.loginfo("Waiting for "+str(self.as_name))
        self.client.wait_for_server()

        # Creates a goal to send to the action server.
        self.goal = GripperCommandGoal()        
    
    def open_gripper(self):

        self.goal.command.position = self.OPEN_VALUE
        self.send_command()
    
    def close_gripper(self):

        self.goal.command.position = self.CLOSE_VALUE
        self.send_command()
    
    def send_command(self):

        client.send_goal(self.goal)
        client.wait_for_result()
        result = client.get_result()
        rospy.loginfo('Action Client result is %s' % str(result))


    def open_close_loop(self):

        while not rospy.is_shutdown():
            self.rate.sleep()
            self.open_gripper()
            self.close_gripper()





if __name__ == '__main__':
    rospy.init_node('dummy_gripper_node')
    RBKairosGripperASClass()
    rospy.spin()