#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Int32

class batteryPublisher(object):

    def __init__(self,time_battery = 5.0):
        self._battery_pub = rospy.Publisher('/robot/battery_level', Int32, queue_size=10)
        self._battery_value = Int32()
        self._max_percentage = 100
        hz = self._max_percentage / time_battery
        rospy.loginfo("Hz="+str(hz))
        self._rate = rospy.Rate(hz)
        self.init_time = rospy.get_time()
        self.now_time = rospy.get_time()

    def start_battery_depletion(self):

        for i in range(self._max_percentage+1):
            self._battery_value.data = self._max_percentage - i
            rospy.loginfo("Battery="+str(self._battery_value.data)+"%")
            self._battery_pub.publish(self._battery_value)
            self._rate.sleep()



if __name__ == '__main__':
    rospy.init_node('talker', anonymous=True)
    batepub_object = batteryPublisher(time_battery = 15.0)
    try:
        batepub_object.start_battery_depletion()
    except rospy.ROSInterruptException:
        pass