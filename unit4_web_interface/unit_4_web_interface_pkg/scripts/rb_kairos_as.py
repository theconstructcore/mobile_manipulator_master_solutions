#! /usr/bin/env python
import rospy
import time
import actionlib

from unit_4_web_interface_pkg.msg import RBKairosFeedback, RBKairosResult, RBKairosAction
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty
from full_pick_place import SendCoordinates, GraspingClient
from geometry_msgs.msg import PoseStamped

class RBKairosClass(object):

    def __init__(self):
        self.label_list = ["table", "table2", "charging_station"]
        self.label_coordinates = {"table":[4.2, 4.0, 0, 0.99],
                                   "table2":[-4, -2, 0.7, -0.7],
                                   "charging_station":[0, 0, 0, 0] }
        # creates the action server
        # create messages that are used to publish feedback/result
        self._feedback = RBKairosFeedback()
        self._result = RBKairosResult()
        self._as = actionlib.SimpleActionServer("/rb_kairos_action_service_as", RBKairosAction, self.goal_callback, False)
        self._as.start()
        self.rate = rospy.Rate(1)
        print 'Action server started'

    def get_label(self,label_index):
        """
        Get the label name based on an index
        """
        try:
            label = self.label_list[int(label_index)]
        except IndexError as ex:
            label = "no-label"

        return label

    def goal_callback(self, goal):
        rospy.loginfo('goal %s received' % str(goal))
        location_to_pickplace = self.get_label(goal.total)
        if location_to_pickplace != "no-label":
            rospy.loginfo("location_to_pickplace="+str(location_to_pickplace))

            # We call the main pipeline
            success = self.start_navigation_pick_and_place_pipeline(pick_place_location_name=location_to_pickplace)
        else:
            rospy.loginfo("NOT VALID location_to_pickplace ="+str(location_to_pickplace))
            success = False

        # return success
        if success:
            print 'result is %s' % str(self._count)
            self._result.result = self._count
            self._as.set_succeeded(self._result)

    def start_navigation_pick_and_place_pipeline(self, pick_place_location_name="table"):

        succeeded = True

        # Make sure sim time is working
        while not rospy.Time.now():
            pass

        # Setup clients
        send_coordinates_object = SendCoordinates(pick_place_location_name)
        time.sleep(10)
        grasping_client = GraspingClient()
        time.sleep(10)

        # Move Arm to navigation Pose
        grasping_client.nav_pose()

        # Move R-Kairos to coordinates
        crd = self.label_coordinates[pick_place_location_name]
        send_coordinates_object.send_goal(crd[0], crd[1], crd[2], crd[3])

        cube_in_grapper = False
        grasping_client.stow()
        i=False
        while not rospy.is_shutdown() and i == False:
            i=True
            #head_action.look_at(1.2, 0.0, 0.0, "base_link")

            # Get block to pick
            fail_ct = 0
            while not rospy.is_shutdown() and not cube_in_grapper:

                # NEW FROM WEB UI Unit
                # We first check that the action wasnt preempted
                if self._as.is_preempt_requested():
                    # cancelled
                    print 'The goal has been cancelled/preempted at %s' % str(self._count)
                    self._as.set_preempted()
                    succeeded = False
                    break
                else:
                    # keep going
                    rospy.loginfo("Picking object...")
                    grasping_client.updateScene()
                    print "Going to Enter getGraspableObject..."
                    cube, grasps = grasping_client.getGraspableObject()
                    print "Exited getGraspableObject..."
                    if cube == None:
                        rospy.logwarn("Perception failed.")
                        # grasping_client.intermediate_stow()
                        grasping_client.stow()
                        #head_action.look_at(1.2, 0.0, 0.0, "base_link")
                        continue

                    # Pick the block
                    print "Call pick function..."
                    if grasping_client.pick(cube, grasps):
                        print "Past pick function"
                        cube_in_grapper = True
                        break
                    rospy.logwarn("Grasping failed.")
                    grasping_client.stow()
                    if fail_ct > 15:
                        fail_ct = 0
                        succeeded = False
                        break
                    fail_ct += 1

            crd = self.label_coordinates["table2"]
            send_coordinates_object.send_goal(crd[0], crd[1], crd[2], crd[3])

            # Place the block
            while not rospy.is_shutdown() and cube_in_grapper:
                # NEW FROM WEB UI Unit
                # We first check that the action wasnt preempted
                if self._as.is_preempt_requested():
                    # cancelled
                    print 'The goal has been cancelled/preempted at %s' % str(self._count)
                    self._as.set_preempted()
                    succeeded = False
                    break
                else:
                    rospy.loginfo("Placing object...")
                    pose = PoseStamped()
                    pose.pose = cube.primitive_poses[0]
                    pose.pose.position.y *= -1.0
                    pose.pose.position.z += 0.02
                    pose.header.frame_id = cube.header.frame_id
                    if grasping_client.place(cube, pose):
                        cube_in_grapper = False
                        break
                    rospy.logwarn("Placing failed.")
                    #grasping_client.intermediate_stow()
                    grasping_client.stow()
                    if fail_ct > 15:
                        fail_ct = 0
                        succeeded = False
                        break
                    fail_ct += 1

            # Tuck the arm, lower the torso
            rospy.loginfo("Finished")

            return succeeded

if __name__ == '__main__':
    rospy.init_node('rb_kairos_action_server_node')
    RBKairosClass()
    rospy.spin()