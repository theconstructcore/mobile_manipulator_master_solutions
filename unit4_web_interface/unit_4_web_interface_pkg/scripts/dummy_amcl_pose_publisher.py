#!/usr/bin/env python

import rospy
import math
import time
from geometry_msgs.msg import PoseWithCovarianceStamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler

class amclPublisher(object):

    def __init__(self, circle_magnitude = 5.0):
        self._amcl_pub = rospy.Publisher('/robot/amcl_pose', PoseWithCovarianceStamped, queue_size=1)
        self._amcl_value = PoseWithCovarianceStamped()
        hz = 5.0
        rospy.loginfo("Hz="+str(hz))
        self._rate = rospy.Rate(hz)
        self._circle_magnitude = circle_magnitude

    def start_amcl_dummy_movement(self):

        while not rospy.is_shutdown():
            for i in range(360):
                if rospy.is_shutdown():
                    break
                rad_angle = math.radians(i)

                self._amcl_value.pose.pose.position.x = self._circle_magnitude * math.sin(rad_angle)
                self._amcl_value.pose.pose.position.y = self._circle_magnitude * math.cos(rad_angle)
                quat = quaternion_from_euler (0.0, 0.0,rad_angle)
                self._amcl_value.pose.pose.orientation.x = quat[0]
                self._amcl_value.pose.pose.orientation.y = quat[1]
                self._amcl_value.pose.pose.orientation.z = quat[2]
                self._amcl_value.pose.pose.orientation.w = quat[3]

                rospy.loginfo("amcl="+str(self._amcl_value.pose.pose)+"%")
                self._amcl_pub.publish(self._amcl_value)
                #self._rate.sleep()
                time.sleep(0.1)



if __name__ == '__main__':
    rospy.init_node('talker', anonymous=True)
    batepub_object = amclPublisher()
    try:
        batepub_object.start_amcl_dummy_movement()
    except rospy.ROSInterruptException:
        pass