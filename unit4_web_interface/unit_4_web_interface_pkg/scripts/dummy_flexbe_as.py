#! /usr/bin/env python
import rospy
import time
import actionlib

from flexbe_msgs.msg import BehaviorExecutionFeedback, BehaviorExecutionResult, BehaviorExecutionAction
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty

class RBKairosClass(object):

    # create messages that are used to publish feedback/result
    _feedback = BehaviorExecutionFeedback()
    _result = BehaviorExecutionResult()

    def __init__(self):
        # creates the action server
        self._as = actionlib.SimpleActionServer("/flexbe/execute_behavior", BehaviorExecutionAction, self.goal_callback, False)
        self._as.start()
        self.rate = rospy.Rate(1)
        rospy.loginfo('Dummy Flexbe Action server started')

    def goal_callback(self, goal):
        behaviour_now = goal.behavior_name
        rospy.loginfo("Behaviour==>"+str(behaviour_now))

        # helper variables
        success = True

        # define vars
        self._count = 0
        max_count = 5
        # perform task
        while self._count < max_count and success and not rospy.is_shutdown():
            rospy.loginfo(self._count)
            self.rate.sleep()
            if self._as.is_preempt_requested():
                # cancelled
                rospy.loginfo('The goal has been cancelled/preempted at %s' % str(self._count))
                self._as.set_preempted()
                success = False
            else:
                # keep going
                self._count = self._count + 1
                self._feedback.current_state = behaviour_now+str(self._count)
                self._as.publish_feedback(self._feedback)

        # return success
        if success:
            self._result.outcome = behaviour_now+"-ENDED"
            rospy.loginfo('result is %s' % str(self._result.outcome))
            self._as.set_succeeded(self._result)

if __name__ == '__main__':
    rospy.init_node('dummy_flexby_node')
    RBKairosClass()
    rospy.spin()