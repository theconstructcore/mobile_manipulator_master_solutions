#!/usr/bin/env python

import copy
import actionlib
import rospy
import time
import copy

from math import sin, cos
from moveit_python import (MoveGroupInterface,
                           PlanningSceneInterface,
                           PickPlaceInterface)
from moveit_python.geometry import rotate_pose_msg_by_euler_angles

from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from grasping_msgs.msg import FindGraspableObjectsAction, FindGraspableObjectsGoal
from geometry_msgs.msg import PoseStamped
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from moveit_msgs.msg import PlaceLocation, MoveItErrorCodes
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint


# Tools for grasping


class GraspingClient(object):

    def __init__(self):
        self.scene = PlanningSceneInterface("robot_base_footprint")
        self.pickplace = PickPlaceInterface("arm", "gripper", verbose=True)
        self.move_group = MoveGroupInterface("arm", "robot_base_footprint")

        find_topic = "basic_grasping_perception/find_objects"
        rospy.loginfo("Waiting for %s..." % find_topic)
        self.find_client = actionlib.SimpleActionClient(
            find_topic, FindGraspableObjectsAction)
        self.find_client.wait_for_server()

    def updateScene(self):
        # find objects
        print "Entered update scene"
        goal = FindGraspableObjectsGoal()
        goal.plan_grasps = True
        self.find_client.send_goal(goal)
        self.find_client.wait_for_result(rospy.Duration(5.0))
        find_result = self.find_client.get_result()

        # remove previous objects
        for name in self.scene.getKnownCollisionObjects():
            self.scene.removeCollisionObject(name, False)
            print "removed old object from scene"
        for name in self.scene.getKnownAttachedObjects():
            self.scene.removeAttachedObject(name, False)
            print "removed old surface from scene"
        self.scene.waitForSync()

        # insert objects to scene
        objects = list()
        idx = -1
        for obj in find_result.objects:
            print "Entered objects loop"
            idx += 1
            obj.object.name = "object%d" % idx
            print "Adding object to scene:"
            print obj.object.name
            print obj.object.primitive_poses[0].position.x
            self.scene.addSolidPrimitive(obj.object.name,
                                         obj.object.primitives[0],
                                         obj.object.primitive_poses[0],
                                         use_service=False)
            #if obj.object.primitive_poses[0].position.x < 0.85:
            if obj.object.primitive_poses[0].position.x < 1.2:
                print "Append to objects"
                objects.append([obj, obj.object.primitive_poses[0].position.z])

        for obj in find_result.support_surfaces:
            # extend surface to floor, and make wider since we have narrow field of view
            height = obj.primitive_poses[0].position.z
            obj.primitives[0].dimensions = [obj.primitives[0].dimensions[0],
                                            1.5,  # wider
                                            obj.primitives[0].dimensions[2] + height]
            obj.primitive_poses[0].position.z += -height/2.0

            # add to scene
            print "Adding surface to scene:"
            self.scene.addSolidPrimitive(obj.name,
                                         obj.primitives[0],
                                         obj.primitive_poses[0],
                                         use_service=False)

        self.scene.waitForSync()

        # store for grasping
        #self.objects = find_result.objects
        self.surfaces = find_result.support_surfaces

        # store graspable objects by Z
        objects.sort(key=lambda object: object[1])
        objects.reverse()
        self.objects = [object[0] for object in objects]
        print "Printing objects..."
        for object in objects:
           print(object[0].object.name, object[1])
        # exit(-1)

    def getGraspableObject(self):
        print "Entered getGraspableObject"
        graspable = None
        for obj in self.objects:
            print "Entered for loop"
            # need grasps
            if len(obj.grasps) < 1:
                print "less than 1 grasp"
                continue
            # check size
            if obj.object.primitives[0].dimensions[0] < 0.03 or \
               obj.object.primitives[0].dimensions[0] > 0.25 or \
               obj.object.primitives[0].dimensions[0] < 0.03 or \
               obj.object.primitives[0].dimensions[0] > 0.25 or \
               obj.object.primitives[0].dimensions[0] < 0.03 or \
               obj.object.primitives[0].dimensions[0] > 0.25:
                print "Incorrect dimensions"
                continue
            # has to be on table
            #if obj.object.primitive_poses[0].position.z < 0.5:
            if obj.object.primitive_poses[0].position.z < 0.3:
                print "Not high enough"
                continue
            print obj.object.primitive_poses[0], obj.object.primitives[0]
            return obj.object, obj.grasps
        # nothing detectedsurface
        return None, None

    def getGraspableObjectV2(self):
        """
        It now returns an array of objects that pass the filter
        """
        print "Entered getGraspableObject"
        object_list = []
        grasps_list = []
        graspable = None
        for obj in self.objects:
            print "Entered for loop"
            # need grasps
            if len(obj.grasps) < 1:
                print "less than 1 grasp"
                continue
            # check size
            if obj.object.primitives[0].dimensions[0] < 0.03 or \
               obj.object.primitives[0].dimensions[0] > 0.25 or \
               obj.object.primitives[0].dimensions[0] < 0.03 or \
               obj.object.primitives[0].dimensions[0] > 0.25 or \
               obj.object.primitives[0].dimensions[0] < 0.03 or \
               obj.object.primitives[0].dimensions[0] > 0.25:
                print "Incorrect dimensions"
                continue
            # has to be on table
            #if obj.object.primitive_poses[0].position.z < 0.5:
            if obj.object.primitive_poses[0].position.z < 0.3:
                print "Not high enough"
                continue
            print obj.object.primitive_poses[0], obj.object.primitives[0]
            object_list.append(obj.object)
            grasps_list.append(obj.grasps)
        # nothing detectedsurface
        rospy.loginfo("Detected ["+str(len(object_list))+"] Objects")
        rospy.loginfo("Detected ["+str(len(grasps_list))+"] Grasps")

        return object_list, grasps_list

    def getSupportSurface(self, name):
        for surface in self.support_surfaces:
            if surface.name == name:
                return surface
        return None

    def getPlaceLocation(self):
        pass

    def pick(self, block, grasps):
        success, pick_result = self.pickplace.pick_with_retry(block.name,
                                                              grasps,
                                                              support_name=block.support_surface,
                                                              scene=self.scene)
        self.pick_result = pick_result
        return success

    def place(self, block, pose_stamped):
        places = list()
        l = PlaceLocation()
        l.place_pose.pose = pose_stamped.pose
        l.place_pose.header.frame_id = pose_stamped.header.frame_id

        # copy the posture, approach and retreat from the grasp used
        l.post_place_posture = self.pick_result.grasp.pre_grasp_posture
        l.pre_place_approach = self.pick_result.grasp.pre_grasp_approach
        l.post_place_retreat = self.pick_result.grasp.post_grasp_retreat
        places.append(copy.deepcopy(l))
        # create another several places, rotate each by 360/m degrees in yaw direction
        m = 16  # number of possible place poses
        pi = 3.141592653589
        for i in range(0, m-1):
            l.place_pose.pose = rotate_pose_msg_by_euler_angles(
                l.place_pose.pose, 0, 0, 2 * pi / m)
            places.append(copy.deepcopy(l))

        success, place_result = self.pickplace.place_with_retry(block.name,
                                                                places,
                                                                scene=self.scene)
        return success

    def tuck(self):
        joints = ["rbkairos_ur10_elbow_joint", "rbkairos_ur10_shoulder_lift_joint", "rbkairos_ur10_shoulder_pan_joint",
                  "rbkairos_ur10_wrist_1_joint", "rbkairos_ur10_wrist_2_joint", "rbkairos_ur10_wrist_3_joint"]
        pose = [2.58, -2.95, -2.26, 0.38, 1.51, -1.57]
        while not rospy.is_shutdown():
            result = self.move_group.moveToJointPosition(joints, pose, 0.02)
            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                return

    def stow(self):
        joints = ["robot_arm_elbow_joint", "robot_arm_shoulder_lift_joint", "robot_arm_shoulder_pan_joint",
                  "robot_arm_wrist_1_joint", "robot_arm_wrist_2_joint", "robot_arm_wrist_3_joint"]

        pose = [2.32, -2.89, -2.32, -2.01, -1.51, 1.54]
        while not rospy.is_shutdown():
            result = self.move_group.moveToJointPosition(joints, pose, 0.02)
            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                return

    def intermediate_stow(self):
        joints = ["shoulder_pan_joint", "shoulder_lift_joint", "upperarm_roll_joint",
                  "elbow_flex_joint", "forearm_roll_joint", "wrist_flex_joint", "wrist_roll_joint"]
        pose = [0.7, -0.3, 0.0, -0.3, 0.0, -0.57, 0.0]
        while not rospy.is_shutdown():
            result = self.move_group.moveToJointPosition(joints, pose, 0.02)
            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                return



def pick_and_place_one_cube():
    # Create a node
    rospy.init_node("demo")

    # Make sure sim time is working
    while not rospy.Time.now():
        pass

    # Setup clients
    time.sleep(10)
    grasping_client = GraspingClient()
    time.sleep(10)

    # Point the head at the cube we want to pick
    cube_in_grapper = False
    grasping_client.stow()
    i=False
    while not rospy.is_shutdown() and i == False:
        i=True

        # Get block to pick
        fail_ct = 0
        while not rospy.is_shutdown() and not cube_in_grapper:
            rospy.loginfo("Picking object...")
            grasping_client.updateScene()
            print "Going to Enter getGraspableObject..."
            cube, grasps = grasping_client.getGraspableObject()
            print "Exited getGraspableObject..."
            if cube == None:
                rospy.logwarn("Perception failed.")
                # grasping_client.intermediate_stow()
                grasping_client.stow()
                continue

            # Pick the block
            print "Call pick function..."
            if grasping_client.pick(cube, grasps):
                print "Past pick function"
                cube_in_grapper = True
                break
            rospy.logwarn("Grasping failed.")
            grasping_client.stow()
            if fail_ct > 15:
                fail_ct = 0
                break
            fail_ct += 1

        # Tuck the arm
        # grasping_client.tuck()

        # Place the block
        while not rospy.is_shutdown() and cube_in_grapper:
            rospy.loginfo("Placing object...")
            pose = PoseStamped()
            pose.pose = cube.primitive_poses[0]
            pose.pose.position.y += -0.2
            pose.pose.position.z += 0.02
            pose.header.frame_id = cube.header.frame_id
            if grasping_client.place(cube, pose):
                cube_in_grapper = False
                break
            rospy.logwarn("Placing failed.")
            #grasping_client.intermediate_stow()
            grasping_client.stow()
            if fail_ct > 15:
                fail_ct = 0
                break
            fail_ct += 1
        # Tuck the arm, lower the torso
        #grasping_client.intermediate_stow()
        #grasping_client.stow()
        rospy.loginfo("Finished")


def pick_and_stack_two_cubes():
    # Create a node
    rospy.init_node("pick_and_stack_two_cubes_node")

    cube_size = 0.04

    # Make sure sim time is working
    while not rospy.Time.now():
        pass

    # Setup clients
    time.sleep(10)
    grasping_client = GraspingClient()
    time.sleep(10)

    # Move Robot arm to the percetion pose
    grasping_client.stow()

    # Get Position of two blocks to stack and
    # Get block to pick
    two_cubes_detected = False
    perception_fail_ct = 0
    while not rospy.is_shutdown() and not two_cubes_detected:
        rospy.loginfo("Picking object...")
        grasping_client.updateScene()
        print "Going to Enter getGraspableObject..."
        cube_list, grasps_list = grasping_client.getGraspableObjectV2()
        print "Exited getGraspableObject..."
        if len(cube_list) >= 2:
            two_cubes_detected = True
        else:
            # We have to retry to get graspable objects again
            rospy.logwarn("Perception failed Times="+str(perception_fail_ct))
            perception_fail_ct += 1

        if perception_fail_ct > 15:
            break

    if two_cubes_detected:
        # Two or more cubes were found, lets try to grasp them
        # Pick the block
        rospy.loginfo("Two cubes detected, lets start STACKING!")


        pose_first_cube = None
        cube_number = 0
        for cube in cube_list:
            grasps = grasps_list[cube_number]
            rospy.loginfo("Starting Pick and place of Cube number ==>"+str(cube_number))
            cube_picked = False
            grasping_fail_ct = 0

            while not rospy.is_shutdown() and not cube_picked:
                rospy.loginfo("Call pick function...")
                if grasping_client.pick(cube, grasps):
                    rospy.loginfo("Past pick function")
                    cube_picked = True
                else:
                    grasping_fail_ct += 1
                    rospy.logwarn("Grasping failed Times="+str(grasping_fail_ct))
                    grasping_client.stow()

                if grasping_fail_ct > 15:
                    break

            if cube_picked:
                cube_placed = False
                placing_fail_ct = 0

                # Place the block
                while not rospy.is_shutdown() and not cube_placed:
                    # We check if we have previously placed a cube
                    if pose_first_cube:
                        rospy.loginfo("First cube placed, so this cube will be stacked")
                        pose = copy.deepcopy(pose_first_cube)
                        # We stack it using the height of the cube plus the normal security offset
                        pose.pose.position.z += cube_size + 0.02
                    else:
                        pose = PoseStamped()
                        pose.pose = cube.primitive_poses[0]
                        pose.pose.position.z += 0.02
                        pose.header.frame_id = cube.header.frame_id
                        rospy.logwarn("Saving Pose of first cube for second cube stacking...")
                        pose_first_cube = copy.deepcopy(pose)

                    rospy.loginfo("Placing object...")

                    if grasping_client.place(cube, pose):
                        cube_placed = True


                    else:
                        placing_fail_ct += 1
                        rospy.logwarn("Placing failed Times="+str(placing_fail_ct))
                        grasping_client.stow()
                    if placing_fail_ct > 15:
                        break
            else:
                rospy.logerr("I dont have any cube picked...")

            rospy.loginfo("FNISHED Pick and place of Cube number ==>"+str(cube_number))
            cube_number += 1

if __name__ == "__main__":
    pick_and_stack_two_cubes()
