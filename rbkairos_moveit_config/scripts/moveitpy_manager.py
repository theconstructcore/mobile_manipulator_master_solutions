#! /usr/bin/env python

import rospy
from moveit_python import MoveGroupInterface
from moveit_msgs.msg import MoveItErrorCodes
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion


class KairosMoveitManager(object):

    def __init__(self):

        self.base_frame = 'robot_base_link'
        self.move_group = MoveGroupInterface("arm", self.base_frame)
        self.gripper_frame = 'robot_arm_tool0'


        self.gripper_yes = [Pose(Point(0.046, 0.0, 1.18), Quaternion(0.0, 0.0, 0.0, 1.0)),
                        Pose(Point(0.027, 0.25, 1.32), Quaternion(0.0, 0.0, 0.7, 0.7))]

        self.gripper_no = [Pose(Point(0.046, 0.0, 1.18), Quaternion(0.0, 0.0, 0.0, 1.0)),
                        Pose(Point(0.027, 0.25, 1.32), Quaternion(0.0, 0.0, 0.7, 0.7))]


    def move_end_effector(self, pose_target):

        gripper_pose_stamped = PoseStamped()
        gripper_pose_stamped.header.frame_id = self.base_frame

        gripper_pose_stamped.header.stamp = rospy.Time.now()

        gripper_pose_stamped.pose = pose_target

        result = self.move_group.moveToPose(gripper_pose_stamped, self.gripper_frame)
        result_message = None
        if result:

            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                rospy.loginfo("Trajectory successfully executed!")
                result_message = "succeeded"
            else:
                state = self.move_group.get_move_action().get_state()
                rospy.logerr("Arm goal in state: %s",state)
                result_message = state

        else:
            rospy.logerr("MoveIt failure! No result returned.")
            result_message = "failure"

        return result_message

    def move_pose_sequence(self, gripper_poses):

        for pose in gripper_poses:
            result_message = self.move_end_effector(pose_target=pose)

    def say_yes(self):
        rospy.loginfo("Saying YES...")
        self.move_pose_sequence(self.gripper_yes)

    def say_no(self):
        rospy.loginfo("Saying NO...")
        self.move_pose_sequence(self.gripper_no)

    def __del__(self):

        self.move_group.get_move_action().cancel_all_goals()


if __name__ == "__main__":
    rospy.init_node('moveit_python_manager', anonymous=True, log_level=rospy.INFO)
    kairos = KairosMoveitManager()
    kairos.say_yes()
    kairos.say_no()
